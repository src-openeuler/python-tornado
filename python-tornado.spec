%global _empty_manifest_terminate_build 0
Name:		python-tornado
Version:	6.4.1
Release:	2
Summary:	Tornado is a Python web framework and asynchronous networking library, originally developed at FriendFeed.
License:	Apache-2.0
URL:		http://www.tornadoweb.org/
Source0:	https://files.pythonhosted.org/packages/source/t/tornado/tornado-%{version}.tar.gz

Patch3000: CVE-2024-52804.patch

%description
Tornado is an open source version of the scalable, non-blocking web server and tools.

%package -n python3-tornado
Summary:	Tornado is a Python web framework and asynchronous networking library, originally developed at FriendFeed.
Provides:	python-tornado
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-cffi
BuildRequires:  python3-pycurl
BuildRequires:	gcc
BuildRequires:	gdb

%description -n python3-tornado
Tornado is an open source version of the scalable, non-blocking web server and tools.

%package help
Summary:	Development documents and examples for tornado
Provides:	python3-tornado-doc

%description help
Tornado is an open source version of the scalable, non-blocking web server and tools.

%prep
%autosetup -n tornado-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .


%files -n python3-tornado -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Nov 26 2024 liyajie <liyajie15@h-partners.com> - 6.4.1-2
- Fix CVE-2024-52804

* Wed Jun 19 2024 yaoxin <yao_xin001@hoperun.com> - 6.4.1-1
- Update to 6.4.1
  * Parsing of the ``Transfer-Encoding`` header is now stricter.
  * Handling of whitespace in headers now matches the RFC more closely.
  * ``tornado.curl_httpclient`` now prohibits carriage return and linefeed headers in HTTP headers
  (matching the behavior of ``simple_httpclient``).

* Fri Oct 13 2023 wen-minjuan <mjwen@isoftstone.com> - 6.3.3-1
- Upgrade version to 6.3.3-1

* Thu Jul 13 2023 liuyongshuai <ysliuci@isoftstone.com> - 6.3.2-1
- Upgrade version to 6.3.2-1

* Fri Jun 16 2023 yaoxin <yao_xin001@hoperun.com> - 6.1-2
- Fix CVE-2023-28370

* Thu Jul 08 2021 yaozc701 <yaozc7@foxmail.com> - 6.1-1
- Upgrade version to 6.1

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 5.0.2-7
- Completing build dependencies

* Fri Sep 11 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 5.0.2-6
- Remove python2-tornado subpackage

* Tue Dec 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.0.2-5
- Package init
